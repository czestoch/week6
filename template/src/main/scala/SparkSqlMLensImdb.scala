import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.rdd._

object MLensImdb {
//------------------------------------------------------------------------------
    def main(args: Array[String]) = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)

        val spark = SparkSession.builder
                                 .appName("MLens + IMDb").getOrCreate()
        run(loadData(spark))
        spark.stop()
    }

//------------------------------------------------------------------------------
    def run(collections: (DataFrame, DataFrame, DataFrame, DataFrame,
                          DataFrame, DataFrame, DataFrame, DataFrame)) = {
        val (ratings, movies, genomeScores, genomeTags,
             links, imdbRatings, imdbPrincipals, imdbNames) = collections
        val best = hundredBest(ratings)
        questionA(best, movies)
        questionB(best, genomeScores, genomeTags)
        questionC(best, ratings, movies)

        val bestLinkedImdb =
                best.join(links, "movieId")
                    .withColumn("imdbId", format_string("tt%07d", col("imdbId")))
        questionD(bestLinkedImdb, imdbRatings, movies)
        questionE(bestLinkedImdb, imdbPrincipals, imdbNames)
    }

//------------------------------------------------------------------------------
    def questionA(best: DataFrame, movies: DataFrame) = {
        ???
    }


//------------------------------------------------------------------------------
    def questionB(best: DataFrame, genomeScores: DataFrame,
                  genomeTags: DataFrame) = {
        val w = Window.partitionBy("movieId").orderBy(desc("relevance"))
        val hundredMostRelevant = 
              best.join(genomeScores, "movieId")
                  .withColumn("rn", row_number.over(w)).where(col("rn") <= 100)
        ???
    }

//------------------------------------------------------------------------------
    def questionC(best: DataFrame, ratings: DataFrame, movies: DataFrame) = {
        ???
    }

//------------------------------------------------------------------------------
    def questionD(bestLinkedImdb: DataFrame, imdbRatings: DataFrame, 
                  movies: DataFrame) = {
        ???
    }

//------------------------------------------------------------------------------
    def questionE(bestLinkedImdb: DataFrame, imdbPrincipals: DataFrame,
                  imdbNames: DataFrame) = {
        ???
    }

//------------------------------------------------------------------------------
    def hundredBest(ratings: DataFrame) = {
        ratings.groupBy("movieId")
               .agg(avg("rating").as("avgRating"), count("rating").as("count"))
               .filter(col("count") >= 30)
               .sort(desc("avgRating"))
               .select("movieId", "avgRating")
               .limit(100)
               .cache
    }

//------------------------------------------------------------------------------
    def loadData(spark: SparkSession) = {
        import spark.implicits._
        val imdbDir = "/cs449/imdb/"
        val mlensDir = "/cs449/movielens/ml-latest/"
        val ratingsFile = mlensDir + "ratings.csv"
        val moviesFile = mlensDir + "movies.csv"
        val genscoresFile = mlensDir + "genome-scores.csv"
        val gentagsFile = mlensDir + "genome-tags.csv"
        val linksFile = mlensDir + "links.csv"
        val imdbratingsFile = imdbDir + "title.ratings.tsv"
        val imdbprincipalsFile = imdbDir + "title.principals.tsv"
        val imdbnamesFile = imdbDir + "name.basics.tsv"
        val opts = Map("header" -> "true", "inferSchema" -> "true")
        val optsTab = opts + ("sep" -> "\t")
        (spark.read.options(opts).csv(ratingsFile).as("Ratings"),
         spark.read.options(opts).csv(moviesFile).as("Movies"),
         spark.read.options(opts).csv(genscoresFile).as("GenomeScores"),
         spark.read.options(opts).csv(gentagsFile).as("GenomeTags"),
         spark.read.options(opts).csv(linksFile).as("MLensLinks"),
         spark.read.options(optsTab).csv(imdbratingsFile).as("ImdbRatings"),
         spark.read.options(optsTab).csv(imdbprincipalsFile).as("ImdbPrincipals"),
         spark.read.options(optsTab).csv(imdbnamesFile).as("ImdbNames")
        )
    }
}
