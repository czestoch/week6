import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.rdd._

object MLensImdb {
//------------------------------------------------------------------------------
    def main(args: Array[String]) = {
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)

        val spark = SparkSession.builder
                                 .appName("MLens + IMDb").getOrCreate()
        run(loadData(spark))
        spark.stop()
    }

//------------------------------------------------------------------------------
    def run(collections: (DataFrame, DataFrame, DataFrame, DataFrame,
                          DataFrame, DataFrame, DataFrame, DataFrame)) = {
        val (ratings, movies, genomeScores, genomeTags,
             links, imdbRatings, imdbPrincipals, imdbNames) = collections
        val best = hundredBest(ratings)
        questionA(best, movies)
        questionB(best, genomeScores, genomeTags)
        questionC(best, ratings, movies)

        val bestLinkedImdb =
                best.join(links, "movieId")
                    .withColumn("imdbId", format_string("tt%07d", col("imdbId")))
        questionD(bestLinkedImdb, imdbRatings, movies)
        questionE(bestLinkedImdb, imdbPrincipals, imdbNames, movies)
    }

//------------------------------------------------------------------------------
    def questionA(best: DataFrame, movies: DataFrame) = {
        val bestTitles =
                best.join(movies, "movieId")
                    .sort(desc("avgRating"))
                    .select("title")
        println("100 best rated movies with at least 30 ratings")
        bestTitles.collect.foreach(println)
        println("++++++++++++++++++++++++++++++++++++++++++++++")
    }


//------------------------------------------------------------------------------
    def questionB(best: DataFrame, genomeScores: DataFrame,
                  genomeTags: DataFrame) = {
        val w = Window.partitionBy("movieId").orderBy(desc("relevance"))
        val hundredMostRelevant = 
              best.join(genomeScores, "movieId")
                  .withColumn("rn", row_number.over(w)).where(col("rn") <= 100)
        val frequentTags =
              hundredMostRelevant
                  .groupBy("tagId")
                  .agg(count("tagId").as("count"))
                  .sort(desc("count"))
                  .limit(15)
        println("15 most frequent tags out of 100 most relevant tags per movie in the 100 best rated movies")
        frequentTags.join(genomeTags, "tagId")
                    .sort(desc("count"))
                    .select("tag")
                    .collect.foreach(println)
        println("++++++++++++++++++++++++++++++++++++++++++++++")
    }

//------------------------------------------------------------------------------
    def questionC(best: DataFrame, ratings: DataFrame, movies: DataFrame) = {
        val mostActive =
                ratings.groupBy("userId")
                       .agg(count("rating").as("count"))
                       .sort(desc("count"))
                       .select("userId")
                       .limit(1000)
        println("Best rated movies by the 1000 most active users which are not in the 100 best movies rated by everyone")
        hundredBest(mostActive.join(ratings, "userId"))
            .select("movieId")
            .except(best.select("movieId"))
            .join(movies, "movieId")
            .select("title")
            .collect.foreach(println)
        println("++++++++++++++++++++++++++++++++++++++++++++++")
    }

//------------------------------------------------------------------------------
    def questionD(bestLinkedImdb: DataFrame, imdbRatings: DataFrame, 
                  movies: DataFrame) = {
        println("Best rated movies in MovieLens ordered by their IMDb average score")
        bestLinkedImdb.join(imdbRatings, col("imdbId") === col("tconst"))
                      .join(movies, "movieId")
                      .sort("averageRating")
                      .select(col("title"),
                              col("avgRating").as("MLensRating"),
                              col("averageRating").as("ImdbRating"))
                      .collect.foreach(println)
        println("++++++++++++++++++++++++++++++++++++++++++++++")
    }

//------------------------------------------------------------------------------
    def questionE(bestLinkedImdb: DataFrame, imdbPrincipals: DataFrame,
                  imdbNames: DataFrame, movies: DataFrame) = {
        println("Actresses or actors with more than 2 acting roles among the 100 best movies in MLens")
        val actingPeople =
                bestLinkedImdb
                    .join(imdbPrincipals, bestLinkedImdb("imdbId") === 
                                                       imdbPrincipals("tconst"))
                    .where(col("category") === "actress" ||
                                                    col("category") === "actor")
                    .cache

        val mostFrequent =
                actingPeople
                    .groupBy("nconst")
                    .agg(countDistinct("movieId").as("count"))
                    .where(col("count") > 2)
                    .cache

        mostFrequent
            .join(imdbNames, "nconst")
            .select("primaryName", "count")
            .collect.foreach(println)
        println("++++++++++++++++++++++++++++++++++++++++++++++")
        println("BONUS: The films they acted in")
        println("++++++++++++++++++++++++++++++++++++++++++++++")
        actingPeople.join(mostFrequent, "nconst")
                    .select("nconst", "movieId")
                    .join(movies, "movieId")
                    .join(imdbNames, "nconst")
                    .select("primaryName", "title")
                    .collect.foreach(println)
        println("++++++++++++++++++++++++++++++++++++++++++++++")
    }

//------------------------------------------------------------------------------
    def hundredBest(ratings: DataFrame) = {
        ratings.groupBy("movieId")
               .agg(avg("rating").as("avgRating"), count("rating").as("count"))
               .filter(col("count") >= 30)
               .sort(desc("avgRating"))
               .select("movieId", "avgRating")
               .limit(100)
               .cache
    }

//------------------------------------------------------------------------------
    def loadData(spark: SparkSession) = {
        import spark.implicits._
        val imdbDir = "/cs449/imdb/"
        val mlensDir = "/cs449/movielens/ml-latest/"
        val ratingsFile = mlensDir + "ratings.csv"
        val moviesFile = mlensDir + "movies.csv"
        val genscoresFile = mlensDir + "genome-scores.csv"
        val gentagsFile = mlensDir + "genome-tags.csv"
        val linksFile = mlensDir + "links.csv"
        val imdbratingsFile = imdbDir + "title.ratings.tsv"
        val imdbprincipalsFile = imdbDir + "title.principals.tsv"
        val imdbnamesFile = imdbDir + "name.basics.tsv"
        val opts = Map("header" -> "true", "inferSchema" -> "true")
        val optsTab = opts + ("sep" -> "\t")
        (spark.read.options(opts).csv(ratingsFile).as("Ratings"),
         spark.read.options(opts).csv(moviesFile).as("Movies"),
         spark.read.options(opts).csv(genscoresFile).as("GenomeScores"),
         spark.read.options(opts).csv(gentagsFile).as("GenomeTags"),
         spark.read.options(opts).csv(linksFile).as("MLensLinks"),
         spark.read.options(optsTab).csv(imdbratingsFile).as("ImdbRatings"),
         spark.read.options(optsTab).csv(imdbprincipalsFile).as("ImdbPrincipals"),
         spark.read.options(optsTab).csv(imdbnamesFile).as("ImdbNames")
        )
    }
}
